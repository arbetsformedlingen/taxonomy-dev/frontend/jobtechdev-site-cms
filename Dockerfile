FROM php:8.0.3-apache
RUN sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
RUN sed -i 's/80/8080/' /etc/apache2/ports.conf
RUN sed -i 's/80/8080/' /etc/apache2/sites-enabled/000-default.conf
RUN cat /etc/apache2/ports.conf
RUN apt-get update
RUN apt-get install -y wget unzip cron
RUN apt-get install -y \
        libzip-dev \
        zip \
    && docker-php-ext-install zip
RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd
RUN wget https://getgrav.org/download/core/grav-admin/1.7.9
RUN unzip 1.7.9 -d /var/www/html/
RUN mv /var/www/html/grav-admin/* /var/www/html/
RUN a2enmod rewrite
RUN mv /var/www/html/webserver-configs/htaccess.txt /var/www/html/.htaccess
RUN chown -R www-data:www-data /var/www/html/ 
RUN chmod 777 -R /var/www/html/
EXPOSE 8080
